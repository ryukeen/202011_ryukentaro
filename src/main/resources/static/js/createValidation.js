//登録ボタンを押下時にチェック実行
function createCheck() {
  const form = document.getElementById('signform');
  //エラーメッセージ
  const login_id_feedback = document.getElementById('login_id_feedback');
  const name_feedback = document.getElementById('name_feedback');
  const password_feedback = document.getElementById('password_feedback');
  const confirmation_passwordd_feedback = document.getElementById('confirmation_passwordd_feedback');
  //バリデーションパターン
  const loginIdPattern = /^[a-zA-Z0-9]{6,20}$/;
  const namePattern = 10;
  const passwordPattern = /^[a-zA-Z0-9 -~]{6,20}$/;

  //エラー数カウント初期値
  const countNumber = 0;

  //エラー数をcountに代入
  let count = countNumber;

  //login_id チェック
  const userLogin_id = form.login_id.value;
  if (userLogin_id == "") {
    login_id_feedback.textContent = 'ログインIDは必須です';
    count++
  } else if (!loginIdPattern.test(userLogin_id)) {
    login_id_feedback.textContent = 'ログインIDは半角数字、6文字以上20文字以下で入力してください';
    count++
  }
  //name チェック
  const userName = form.name.value;
  if (userName == "") {
    name_feedback.textContent = '名前は必須です';
    count++
  } else if (userName.length >= namePattern) {
    name_feedback.textContent = '名前は10文字以下で入力してください';
    count++
  }
  //password チェック
  const userPassword = form.password.value;
  if (userPassword == "") {
    password_feedback.textContent = 'パスワードは必須です';
    count++
  } else if (!passwordPattern.test(userPassword)) {
    password_feedback.textContent = 'パスワードは半角数字、6文字以上20文字以下で入力してください';
    count++
  }
  //パスワードと確認用パスワード チェック
  const userConfirmationPassword = form.confirmationPassword.value;
  if (userConfirmationPassword != userPassword) {
    confirmation_passwordd_feedback.textContent = 'パスワードと確認用パスワードが一致しません';
    count++
  }
  //エラーの有無 チェック
  if (count != countNumber) {
    return false;
  }
};
package jp.alhinc.springtraining.form;

import lombok.Data;

@Data
public class EditUserForm {

	public int id;

//	@NotEmpty(groups = {Groups1.class}, message = "ログインIDは必須です")
//	@Size(min = 6, max = 20, groups = {Groups2.class}, message = "ログインIDは{min}文字以上{max}文字以下で入力してください")
//	@Pattern(regexp = "^[a-zA-Z0-9]*", groups = {Groups3.class}, message = "ログインIDは半角英数字で入力してください")
	public String login_id;

//	@NotEmpty(groups = {Groups1.class}, message = "名前は必須です")
//	@Size(min = 1, max = 10, groups = {Groups2.class}, message = "名前は{min}文字以上{max}文字以下で入力してください" )
	public String name;

	public String registrationPassword;

//	@AssertTrue(groups = {Groups1.class}, message="記号を含む全ての半角英数字で6文字から20文字で入力して下さい")
//	public boolean isRegistrationPasswordValid() {
//		if((registrationPassword == null || registrationPassword.isEmpty()) && (confirmationPassword == null || confirmationPassword.isEmpty()) ||
//				(registrationPassword.matches("^[a-zA-Z0-9 -~]*$") && registrationPassword.length() >= 6 && registrationPassword.length() <= 20))
//			return true;
//		return false;
//	}

	public String confirmationPassword;

//	@AssertTrue(groups = {Groups1.class}, message="パスワードと確認用パスワードが一致しません")
//	public boolean isPasswordValid() {
//		if(registrationPassword == null || registrationPassword.isEmpty())
//			return true;
//		return registrationPassword.equals(confirmationPassword);
//	}

	public int branch_id;
	public int department_position_id;
}

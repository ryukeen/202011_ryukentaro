package jp.alhinc.springtraining.mapper;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import jp.alhinc.springtraining.entity.User;

@Mapper
public interface UserMapper {

	//全ユーザーの情報を取得
	List<User> findAll();

	//entityの情報(id)をUserMappeer.xmlの"getUser"へ
	User getUser(int id);

	//entityの情報をUserMappeer.xmlの"create"へ
	int create(User entity);

	//entityの情報をUserMappeer.xmlの"edit"へ
	int edit(User entity);

	//entityの情報をUserMappeer.xmlの"stop"へ
	int stop(User entity);

	//entityの情報をUserMappeer.xmlの"start"へ
	int start(User entity);

	//entityの情報(login_id)をUserMappeer.xmlの"getLoginId"へ
	User getLoginId(String login_id);

	//entityの情報(login_id)をUserMappeer.xmlの"getEditLoginId"へ
	User getEditLoginId(int id);
}

package jp.alhinc.springtraining.controller;

import java.util.List;

import javax.swing.text.Position;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import jp.alhinc.springtraining.entity.Branch;
import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.CreateUserForm;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.service.CreateUserService;
import jp.alhinc.springtraining.service.EditUserService;
import jp.alhinc.springtraining.service.GetAllUsersService;
import jp.alhinc.springtraining.service.GetBranchService;
import jp.alhinc.springtraining.service.GetDepartmentPositionService;
import jp.alhinc.springtraining.validation.Validation;

@Controller
@RequestMapping("/users")
public class UsersController {

	@Autowired
	private GetAllUsersService getAllUsersService;

	@Autowired
	private GetBranchService getBranchService;

	@Autowired
	private GetDepartmentPositionService getDepartmentPositionService;

	@Autowired
	private CreateUserService createUserService;

	@Autowired
	private EditUserService editUserService;

//	public static final String INPUT_ERROR = "入力に間違いがあります";
	public static final String ALREADY_LOGIN_ID = "ログインIDは既に存在します";
	public static final String FORM = "form";
	public static final String BRANCHES = "branches";
	public static final String DEPARTMENT_POSITIONS = "departmentPositions";
	public static final String USERS = "users";
	public static final String MESSAGE = "message";

	//---ホーム画面処理---//
	@GetMapping
	public String index(Model model) {
		//全ユーザーの情報を取得
		List<User> users = getAllUsersService.getAllUsers();
		model.addAttribute(USERS, users);
		return "users/index";
	}

	//---ユーザー停止処理---//
	@PostMapping("/stop")
	public String stop(EditUserForm form) {
		//form情報(停止させるユーザーのid)をサービスへ
		editUserService.stop(form);
		return "redirect:/users";
	}

	//---ユーザー復活処理---//
	@PostMapping("/start")
	public String start(EditUserForm form) {
		//form情報(復活させるユーザーのid)をサービスへ
		editUserService.start(form);
		return "redirect:/users";
	}

	//---新規登録処理---//
	@GetMapping("/create")
	public String create(Model model) {
		//支店の情報取得
		List<Branch> branches = getBranchService.getBranch();
		//部署・役職の情報取得
		List<Position> departmentPositions = getDepartmentPositionService.getPosition();
		model.addAttribute(FORM, new CreateUserForm());
		model.addAttribute(BRANCHES, branches);
		model.addAttribute(DEPARTMENT_POSITIONS, departmentPositions);
		return "users/create";
	}

	@PostMapping("/create")
	public String create(@ModelAttribute("form") @Validated(Validation.class) CreateUserForm form, BindingResult result, Model model) {
		//支店の情報取得
		List<Branch> branches = getBranchService.getBranch();
		//部署・役職の情報取得
		List<Position> departmentPositions = getDepartmentPositionService.getPosition();

		//バリデーションエラー時、支店、部署・役職の情報取得
//		if(result.hasErrors()) {
//			model.addAttribute(BRANCHES, branches);
//			model.addAttribute(DEPARTMENT_POSITIONS, departmentPositions);
//			model.addAttribute(MESSAGE, INPUT_ERROR);
//			return "users/create";
//		}

		//ログインIDを元に重複の有無を確認
		boolean isEmpty = createUserService.createConfirmation(form.getLogin_id());

		//trueの場合、重複のエラー表示を行う。
		if(isEmpty == true) {
			model.addAttribute(BRANCHES, branches);
			model.addAttribute(DEPARTMENT_POSITIONS, departmentPositions);
			model.addAttribute(MESSAGE, ALREADY_LOGIN_ID);
			return "users/create";
		} else {
			//formの情報をサービスへ
			createUserService.create(form);
			//ホーム画面に遷移
			return "redirect:/users";
		}
	}

	//---ユーザー編集処理---//
	@GetMapping("/edit")
	public String edit(Model model, @RequestParam int id) {
		//ユーザーidを元にユーザー情報を取得
		EditUserForm form = getAllUsersService.getUsers(id);
		//支店の情報取得
		List<Branch> branches = getBranchService.getBranch();
		//部署・役職の情報取得
		List<Position> departmentPositions = getDepartmentPositionService.getPosition();
		model.addAttribute(FORM, form);
		model.addAttribute(BRANCHES, branches);
		model.addAttribute(DEPARTMENT_POSITIONS, departmentPositions);
		return "users/edit";
	}

	@PostMapping("/edit")
	public String edit(@ModelAttribute("form") @Validated(Validation.class) EditUserForm form, BindingResult result, Model model) {
		//支店の情報取得
		List<Branch> branches = getBranchService.getBranch();
		//部署・役職の情報取得
		List<Position> departmentPositions = getDepartmentPositionService.getPosition();

//		//バリデーションエラー時、支店、部署・役職の情報取得
//		if(result.hasErrors()) {
//			model.addAttribute(BRANCHES, branches);
//			model.addAttribute(DEPARTMENT_POSITIONS, departmentPositions);
//			model.addAttribute(MESSAGE, INPUT_ERROR);
//			return "users/edit";
//		}

		//idまたは、入力したログインIDを元に重複の有無を確認
		boolean isEmpty = editUserService.editConfirmation(form.getId(),form.getLogin_id());

		//trueの場合、重複のエラー表示を行う。
		if(isEmpty == true) {
			model.addAttribute(BRANCHES, branches);
			model.addAttribute(DEPARTMENT_POSITIONS, departmentPositions);
			model.addAttribute(MESSAGE, ALREADY_LOGIN_ID);
			return "users/edit";
		} else {
			//formの情報をサービスへ
			editUserService.edit(form);
			//ホーム画面に遷移
			return "redirect:/users";
		}
	}
}

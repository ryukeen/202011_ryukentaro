package jp.alhinc.springtraining.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.entity.User;
import jp.alhinc.springtraining.form.EditUserForm;
import jp.alhinc.springtraining.mapper.UserMapper;

@Service
public class EditUserService {

	@Autowired
	private UserMapper mapper;

	@Transactional
	//ユーザーの情報をformからentityに詰め替える
	public int edit(EditUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		entity.setLogin_id(form.getLogin_id());
		entity.setName(form.getName());
		entity.setBranch_id(form.getBranch_id());
		entity.setDepartment_position_id(form.getDepartment_position_id());
		//パスワードを暗号化し、entityに詰める
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		entity.setPassword(encoder.encode(form.getRegistrationPassword()));
		return mapper.edit(entity);
	}

	public boolean editConfirmation(int id, String login_id) {
		//登録してあるlogin_idを取得
		User alreadyLoginId = mapper.getLoginId(login_id);
		//idを元に編集者現在のlogin_idを取得
		User nowLoginId = mapper.getEditLoginId(id);
		//入力したlogin_idをセット
		User newLoginId = new User();
		newLoginId.setLogin_id(login_id);
		//現在のlogin_idと入力したlogin_idを比較
		if (nowLoginId.getLogin_id().equals(newLoginId.getLogin_id())) {
			return false;
		} else if (alreadyLoginId == null) {
			//登録してあるlogin_idか確認
			return false;
		} else {
			return true;
		}
	}

	//停止させるユーザーの情報をformからentityに詰め替える
	public int stop(EditUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		return mapper.stop(entity);
	}

	//復活させるユーザーの情報をformからentityに詰め替える
	public int start(EditUserForm form) {
		User entity = new User();
		entity.setId(form.getId());
		return mapper.start(entity);
	}
}

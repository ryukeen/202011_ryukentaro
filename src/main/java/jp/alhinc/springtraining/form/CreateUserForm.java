package jp.alhinc.springtraining.form;

import java.io.Serializable;

import lombok.Data;

@Data
public class CreateUserForm implements Serializable {

	public int id;

//	@NotEmpty(groups = {Groups1.class}, message = "ログインIDは必須です")
//	@Size(min = 6, max = 20, groups = {Groups2.class}, message = "ログインIDは{min}文字以上{max}文字以下で入力してください")
//	@Pattern(regexp = "^[a-zA-Z0-9]*$", groups = {Groups3.class}, message = "ログインIDは半角英数字で入力してください")
	public String login_id;

//	@NotEmpty(groups = {Groups1.class}, message = "名前は必須です")
//	@Size(min = 1, max = 10, groups = {Groups2.class}, message = "名前は{min}文字以上{max}文字以下で入力してください" )
	public String name;

//	@NotEmpty(groups = {Groups1.class}, message = "パスワードは必須です")
//	@Size(min = 6, max = 20, groups = {Groups2.class}, message = "パスワードは{min}文字以上{max}文字以下で入力してください")
//	@Pattern(regexp = "^[a-zA-Z0-9 -~]*$", groups = {Groups3.class}, message = "パスワードは記号を含む半角文字で入力してください")
	public String registrationPassword;

//	@NotEmpty(groups = {Groups1.class}, message = "確認用パスワードは必須です")
	public String confirmationPassword;

//	@AssertTrue(groups = {Groups1.class}, message="パスワードと確認用パスワードが一致しません")
//	public boolean isPasswordValid() {
//		if(registrationPassword == null || registrationPassword.isEmpty())
//			return true;
//		return registrationPassword.equals(confirmationPassword);
//	}

	public int branch_id;
	public int department_position_id;

}

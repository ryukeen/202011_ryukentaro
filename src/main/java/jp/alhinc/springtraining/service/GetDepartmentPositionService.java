package jp.alhinc.springtraining.service;

import java.util.List;

import javax.swing.text.Position;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import jp.alhinc.springtraining.mapper.DepartmentPositionMapper;

@Service
public class GetDepartmentPositionService {

	@Autowired
	private DepartmentPositionMapper mapper;

	@Transactional
	//部署・役職の情報を取得
	public List<Position> getPosition() {
		return mapper.findAll();
	}
}
